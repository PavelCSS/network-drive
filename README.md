# mac-network-drive

## Installation

```
npm install git+https://PavelCSS@bitbucket.org/PavelCSS/network-drive.git
```

## Usage

All examples assume:

```javascript
let networkDrive = require('network-drive');
```

### find

find(drivePath: string): Promise<string | undefined>

#### Examples

```javascript
 networkDrive.find("\\DoesExist\Path")
 .then(function (path)
 {
	 // path === "/Volumes/"
 });

 networkDrive.find("\\\\DoesNOTExist\Path")
 .then(function (path)
 {
	 // path === undefined
 });
```
### list

list(void): Promise<Object>

#### Examples

```javascript
 networkDrive.list()
 .then(function (drives)
 {
	 /*
		drives = {
			"/Volumes/...":"\\DoesExist\Path\Files",
			"/Volumes/...":"\\NETWORKB\\DRIVE C"
		}
	*/
 });
 ```

### mount

mount(drivePath: string, undefined, username?: string, password?: string): Promise<string>

The second param is useless. I kept it to have the same amount of parameters that the windows package.

#### Examples

```javascript
 networkDrive.mount("\\\\DoesExist\\Path\\Files", undefined, undefined, undefined)
 .then(function (path)
 {
	 // path = "/Volumes/xxx"
 });
```

### unmount

Not implemented yet
