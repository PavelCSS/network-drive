import macNetworkDrive from './darwin';
import windowsNetworkDrive from './win32';

export default process.platform === 'darwin' ? macNetworkDrive : windowsNetworkDrive;
