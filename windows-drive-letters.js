import tableParser from 'table-parser';
import childProcess from 'child_process';

const command = 'wmic logicaldisk get caption';

const removeUsedLetters = (usedLetters) => {
  const letters = [
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
    'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
  ];

  for (let i = 0; i < usedLetters.length; i++) {
    letters.splice(letters.indexOf(usedLetters[i]), 1);
  }

  return letters;
};

const letters = () => {
  return new Promise((resolve, reject) => {
    childProcess.exec(command, (err, stdout) => {
      if (err) {
        return reject(err);
      }

      const letters = (
        tableParser
          .parse(stdout)
          .map(caption => caption.Caption[0].replace(':', ''))
      );

      resolve(removeUsedLetters(letters));
    });
  });
};

const usedLetters = () => {
  return new Promise((resolve, reject) => {
    childProcess.exec(command, (err, stdout) => {
      if (err) {
        return reject(err);
      }

      resolve(
        tableParser
          .parse(stdout)
          .map(caption => caption.Caption[0].replace(':', ''))
      );
    });
  });
};

const randomLetter = () => {
  return (
    letters()
      .then((letters) => {
        const index = Math.floor(Math.random() * letters.length);

        return letters[index];
      })
  );
};

const usedLettersSync = () => {
  const stdout = childProcess.execSync(command);

  return (
    tableParser
      .parse(stdout.toString())
      .map(caption => caption.Caption[0].replace(':', ''))
  );
};

const lettersSync = () => {
  const stdout = childProcess.execSync(command);
  const letters = tableParser.parse(stdout.toString()).map(caption => caption.Caption[0].replace(':', ''));

  return removeUsedLetters(letters);
};

const randomLetterSync = () => {
  const letters = lettersSync();
  const index = Math.floor(Math.random() * letters.length);

  return letters[index];
};

export default {
  letters,
  usedLetters,
  lettersSync,
  randomLetter,
  usedLettersSync,
  randomLetterSync,
};
