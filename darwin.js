import applescript from 'applescript';
import { exec } from 'child-process-promise';

const SMB_MOUNT_TYPE = 'smb';
const AFP_MOUNT_TYPE = 'afp';
const MAX_BUFFER_SIZE = 2000 * 1024;

const macNetworkDrive = {
  smbType: SMB_MOUNT_TYPE,
  afpType: AFP_MOUNT_TYPE,

  list() {
    let fetchDisks = exec('/bin/df', {
      maxBuffer: MAX_BUFFER_SIZE
    });

    return (
      fetchDisks
        .then((result) => {
          if (typeof result.stderr === 'string' && result.stderr.length !== 0) {
            return Promise.reject(stderr);
          }

          let pathList = result.stdout.split(/\s*[\n\r]+/g);
          const drivePath = {};
          const regex = /^\/\/\S*@(\S*)\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*(\S*)$/;
          const re = new RegExp(regex, 'i');

          pathList.splice(0, 1);

          for (const line of pathList) {
            const matches = line.match(re);

            if (matches) {
              drivePath[matches[1].replace(/\\/g, '/')] = matches[2];
            }
          }

          return drivePath;
        })
    );
  },

  find(drivePath) {
    if ('string' !== typeof drivePath || 0 === drivePath.length) {
      return Promise.reject('Invalid path');
    }

    return (
      macNetworkDrive
        .list()
        .then((networkDrives) => {
          for (const currentDrivePath in networkDrives) {
            if (!networkDrives.hasOwnProperty(currentDrivePath)) {
              continue;
            }

            if (currentDrivePath.toUpperCase() === drivePath.toUpperCase()) {
              return networkDrives[currentDrivePath];
            }
          }
        })
    );
  },

  mount(drivePath, mountType = SMB_MOUNT_TYPE, username = null, password = null) {
    /* property mountType is unused -> allow to have the same signature with "windows-drive" */
    return (
      new Promise((resolve, reject) => {
        let connectPath = '';
        const serverPath = drivePath;

        if (username != null && password != null) {
          connectPath = `${username}:${password}@`;
        }

        const pathDrive = `${mountType || SMB_MOUNT_TYPE}://${connectPath}${serverPath}`;
        const mountCommand = mountType === SMB_MOUNT_TYPE ? 'mount volume' : 'mount_afp';
        const mountScript = `
          tell application "Finder"
          with timeout of 2 seconds
            try
              ${mountCommand} "${pathDrive}"
            end try
            end timeout
          end tell`;

        applescript.execString(mountScript, (err, result) => {
          if (err || result === undefined) {
            if (err === undefined) {
              return reject('Unable to connect');
            }

            return reject(err)
          }

          return resolve(true)
        })
      })
    );
  },

  unmount: function unmount() {
  }
};

export default macNetworkDrive;
