import path from 'path';
import { exec } from 'child-process-promise';
import windowDriveLetters from './windows-drive-letters';

const MAX_BUFFER_SIZE = 2000 * 1024;

/**
 * @param {string} input - Drive path to search for
 * @returns {void}
 * @description Assertion if the value is a non empty string
 */
const assertIfNonEmptyString = (input) => {
  /**
   * Ignore this in code coverage because it should never happen
   */
  /* istanbul ignore if */
  if ("string" === typeof input && 0 !== input.length) {
    throw (new Error(input));
  }
};

const windowsNetworkDrive = {
  /**
   * @function find
   * @public
   * @param {string} drivePath - Drive path to search for
   * @returns {Promise<string[]>} - An array of drive letters that point to the drive path
   * @description Gets the network drive letter for a path
   * @example
   * networkDrive.find("\\DoesExist\Path")
   * // returns
   * ["T"]
   * @example
   * networkDrive.find("\\DoesNOTExist\Path")
   * // returns
   * []
   */
  find(drivePath) {
    return (
      Promise
        .resolve()
        .then(() => {
          if (false === windowsNetworkDrive.isWinOs()) {
            throw (new Error('windows-network-drive can only run on windows.'));
          }

          /**
           * Param check
           */
          if ("string" !== typeof drivePath || 0 === drivePath.trim().length) {
            throw (new Error('Drive path is not valid. drive path = ' + JSON.stringify(drivePath)));
          }
        })

        .then(() => (
          windowsNetworkDrive
            .pathToWindowsPath(drivePath)
            .then(newPath => (drivePath = newPath))
        ))

        .then(windowsNetworkDrive.list)
        .then((networkDrives) => {
          const driveLetters = [];

          /**
           * Create the list of drives to path
           */
          for (let currentDriveLetter in networkDrives) {
            /**
             * There is not an easy way to test this
             */
            /* istanbul ignore if */
            if (!networkDrives.hasOwnProperty(currentDriveLetter)) {
              continue;
            }

            const currentDrivePath = networkDrives[currentDriveLetter];

            if (currentDrivePath === drivePath) {
              driveLetters.push(currentDriveLetter.toUpperCase());
            }
          }

          return driveLetters;
        })
    );
  },

  /**
   * @function isWinOs
   * @public
   * @returns {boolean} - True if is a Windows OS
   * @description Test the current OS is Windows. This was split into a function for code testing
   * @example
   * if (true ===networkDrive.isWinOs())
   * {
	 *     console.log("This is running on Windows");
	 * }
   */
  isWinOs() {
    return /^win/.test(process.platform);
  },

  /**
   * @function list
   * @public
   * @returns {Promise<Object>} - Object keys are drive letters, values are the network path
   * @description lists all network drives and paths
   * @example
   * networkDrive.list()
   * // returns
   * {
	 *    "F":"\\NETWORKA\Files",
	 *    "K":"\\NETWORKB\DRIVE G"
	 * }
   */
  list() {
    return (
      Promise
        .resolve()
        .then(() => {
          if (false === windowsNetworkDrive.isWinOs()) {
            throw (new Error('windows-network-drive can only run on windows.'));
          }
        })
        .then(() => (
          exec("wmic path Win32_LogicalDisk Where DriveType=\"4\" get DeviceID, ProviderName", {
            maxBuffer: MAX_BUFFER_SIZE,
          })
        ))
        .then((result) => {
          let pathList;
          const drivePaths = {};

          assertIfNonEmptyString(result.stderr);

          /**
           * Break based on the line endings (one for each network drive)
           * and remove the header row
           */
          pathList = result.stdout.split(/\s*[\n\r]+/g);
          pathList.splice(0, 1);

          /**
           * Create the list of drives to path
           */
          for (let currentPathIndex = 0; currentPathIndex < pathList.length; currentPathIndex++) {
            const currentPath = pathList[currentPathIndex];

            if ("string" === typeof currentPath && 0 < currentPath.length) {
              const colonIndex = currentPath.indexOf(':');
              const driveLetter = currentPath.substring(0, colonIndex);
              const drivePath = currentPath.substring(colonIndex + 1);

              drivePaths[driveLetter.trim().toUpperCase()] = drivePath.trim();
            }
          }

          return drivePaths;
        })
    );
  },

  /**
   * @function mount
   * @public
   * @param {string} drivePath - Path to create a network drive to
   * @param {string} [driveLetter] - Drive letter to use when mounting. If undefined a random drive letter will be used.
   * @param {string} [username] - Username to use when accessing network drive
   * @param {string} [password] - Password to use when accessing network drive
   * @returns {Promise<string>} - Drive letter
   * @description Creates a network drive
   * @example
   * networkDrive.mount("\\NETWORKA\Files")
   * // returns
   * "F"
   */
  mount(drivePath, driveLetter, username, password) {
    let mountCommand = "net use";

    return Promise.resolve()
      .then(() => {
        if (false === windowsNetworkDrive.isWinOs()) {
          throw (new Error('windows-network-drive can only run on windows.'));
        }
      })

      /**
       * Param check
       */
      .then(() => {
        if ("string" !== typeof drivePath || 0 === drivePath.trim().length) {
          throw (new Error('Drive path is not valid. drive path = ' + JSON.stringify(drivePath)));
        }

        drivePath = drivePath.trim();

        if ("string" === typeof driveLetter) {
          driveLetter = driveLetter.trim();
        }

        if ("string" !== typeof driveLetter && undefined !== driveLetter) {
          throw (new Error('Drive letter must be a string or undefined'));
        } else if ("" === driveLetter) {
          driveLetter = undefined;
        }

        if ("string" !== typeof username && undefined !== username) {
          throw (new Error('Username must be a string or undefined'));
        } else if ("" === username) {
          username = undefined;
        }

        if ("string" !== typeof password && undefined !== password) {
          throw (new Error('Password must be a string or undefined'));
        } else if ("" === password) {
          password = undefined;
        }
      })

      .then(() => (
        windowsNetworkDrive
          .pathToWindowsPath(drivePath)
          .then(newPath => (drivePath = newPath))
      ))

      /**
       * Get the next drive letter
       */
      .then(driveLetters.randomLetter)
      .then((newDriveLetter) => {
        if (undefined === driveLetter) {
          /**
           * Get a drive letter if one was not given
           */
          driveLetter = newDriveLetter;
        }
      })

      /**
       * Create the command to run
       */
      .then(() => {
        mountCommand += " " + driveLetter + ": \"" + drivePath + "\" /P:Yes";

        /**
         * There is not an easy way to setup a network drive with a username and password
         */
        /* istanbul ignore next */
        if ("string" === typeof username && "string" === typeof password) {
          mountCommand += " /user:" + username + " " + password;
        }
      })

      /**
       * mount the drive
       */
      .then(() => {
        return exec(mountCommand, { maxBuffer: MAX_BUFFER_SIZE });
      })
      .then(result => assertIfNonEmptyString(result.stderr))

      /**
       * Return the drive letter for this mount
       */
      .then(() => driveLetter.toUpperCase())
  },

  /**
   * @function unmount
   * @public
   * @param {string} driveLetter - Drive letter to remove
   * @returns {Promise<void>}
   * @description Removes a network drive
   * @example
   * networkDrive.unmount("F")
   */
  unmount(driveLetter) {
    return Promise.resolve()
      .then(() => {
        if (false === windowsNetworkDrive.isWinOs()) {
          throw (new Error('windows-network-drive can only run on windows.'));
        }
      })

      /**
       * Param check
       */
      .then(() => {
        if ("string" !== typeof driveLetter || 0 === driveLetter.trim().length) {
          throw (new Error('Drive letter is not valid'));
        }

        driveLetter = driveLetter.trim().toUpperCase();
      })

      /**
       * Get the used drive letter
       */
      .then(windowDriveLetters.usedLetters)
      .then((windowDriveLetters) => {
        /**
         * If this drive is mounted, remove it
         */
        if (-1 !== windowDriveLetters.indexOf(driveLetter)) {
          let unmountCommand = "net use " + driveLetter + ": /Delete /y";

          return exec(unmountCommand, { maxBuffer: MAX_BUFFER_SIZE })
            .then(result => assertIfNonEmptyString(result.stderr));
        }
      });
  },

  /**
   * @function pathToWindowsPath
   * @public
   * @param {string} drivePath - Path to be converted
   * @returns {Promise<string>} - Converted path
   * @description Converts a path to a windows path
   * @example
   * networkDrive.pathToWindowsPath('K:/test')
   * // returns
   * 'K:\test'
   */
  pathToWindowsPath(drivePath) {
    return Promise.resolve()
      .then(() => {
        if (false === windowsNetworkDrive.isWinOs()) {
          throw (new Error('windows-network-drive can only run on windows.'));
        }
      })

      /**
       * Param check
       */
      .then(() => {
        if ("string" !== typeof drivePath) {
          throw (new Error('Drive path is not valid. drive path = ' + JSON.stringify(drivePath)));
        }

        drivePath = drivePath.trim();
        if (0 === drivePath.length) {
          throw (new Error('Drive path is not valid. drive path is only whitespace. drive path = ' + JSON.stringify(drivePath)));
        }
      })

      .then(() => {
        drivePath = path.normalize(drivePath);
        drivePath = drivePath.replace('/', '\\');

        /**
         * Remove the trailing \ because it breaks the net use command
         */
        return drivePath.replace(/\\+$/, '');
      });
  }
};

export default windowsNetworkDrive;
